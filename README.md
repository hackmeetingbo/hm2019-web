Hackmeeting 0x7e3
=================

Sitio web para el Hackmeeting 2019 escrito en Markdown y procesado con [Hugo](https://gohugo.io/).

Quiero editar!
--------------

Las páginas estáticas están en /content.

La página de inicio está en themes/hackmeeting-hugo/layouts/index.html.


## Correr en modo desarrollo
Para correr el sitio en modo desarrollo principalmente es descargar el binario de [Hugo](https://github.com/gohugoio/hugo/releases). Una vez instalado ir al directorio del proyecto y ejecutar lo siguiente:

```
$ hugo -D server --watch
```

Lo cual creará un servidor de desarrollo en [http://localhost:1313/](http://localhost:1313/).
