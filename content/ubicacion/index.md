+++
title = "Ubicación"
+++

## Sede: Centro Cultural "Dragón Wari"

El Centro Cultural "Dragón Wari" está ubicado en la esquina de las calles José Saravia y Pioneros de Rochdale, a dos cuadras del Estadio Bolívar (Tembladerani) y la Av. Landaeta cerca al Ex. Cine Ebro.

<a href="https://www.openstreetmap.org/export/embed.html?bbox=-68.14277529716493%2C-16.510308576790766%2C-68.13978195190431%2C-16.508747598462378&amp;layer=mapnik&amp;marker=-16.509526803388088%2C-68.1412786245346" target="_blank">MAPA</a>

## De donde tomar minibuses

Para llegar al <b>Centro Cultural Dragon Wari</b> deben tomar los minibuses que van hacia el Estadio Bolivar subiendo por la Av. Landaeta. Estos minibuses se encuentran parados al frente de la <b>Plaza del Estudiante</b> (iniciando <b>El Prado</b>), el pasaje cuesta Bs. 2 y tarda <b>7min</b> aprox. en llegar al Estadio Bolivar. Hay que bajarse en el Estadio Bolivar en el semáforo e ir dos cuadras hacia la derecha por la calle José Saravia. Igual pueden ver el <a href="https://www.openstreetmap.org/export/embed.html?bbox=-68.14277529716493%2C-16.510308576790766%2C-68.13978195190431%2C-16.508747598462378&amp;layer=mapnik&amp;marker=-16.509526803388088%2C-68.1412786245346" target="_blank">mapa</a> y aprovechar con el GPS de su celuar :).

Donde tomar los minibuses:
<img style="display: block; margin: 0 auto;" src="/img/minibuses1.jpg" />
<img style="display: block; margin: 0 auto;" src="/img/minibuses2.jpg" />

Donde bajarse:

<img style="display: block; margin: 0 auto;" src="/img/tembladerani0.jpg" />
<img style="display: block; margin: 0 auto;" src="/img/tembladerani1.jpg" />

Centro Cultural Dragon Wari!!

<img style="display: block; margin: 0 auto;" src="/img/llama.jpg" alt="Imagen: el Dragón Wari se destaca por una cabeza de llama" />

Mapa:

<iframe style="width: 100%; height: 500px;" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=-68.14277529716493%2C-16.510308576790766%2C-68.13978195190431%2C-16.508747598462378&amp;layer=mapnik&amp;marker=-16.509526803388088%2C-68.1412786245346" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=-16.50953&amp;mlon=-68.14128#map=19/-16.50953/-68.14128">Ver mapa más grande</a></small>
