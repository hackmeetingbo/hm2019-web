+++
# date = "2016-11-06T13:00:25+05:30"
title = "Contacto"
+++

## Para asistentes

Aún no anunciamos los canales de contacto y redes sociales oficiales, pero puedes contactarte con la organización a la dirección de correo:

hackmeeting [@] hackmeeting.org.bo.

Tenemos clave GPG también: [D881 B9DA 3E4E CA18 3873 DB9E 855D B92F 24B9 BDC6](https://pgp.mit.edu/pks/lookup?search=0x855DB92F24B9BDC6&op=vindex).
<!--
## Para expositores y postulantes al CFP

Más información en [la página del CFP]({ {< ref "cfp" >} }).

El contacto directo a la comisión del CFP está en el correo cfp [@] hackmeeting.org.bo.

La clave GPG es [D107 D039 1F07 2910 F634 B861 35BC 0BF4 ECE9 0A95](https://pgp.mit.edu/pks/lookup?search=0x35BC0BF4ECE90A95&op=vindex). -->
