+++
# date = "2019-06-07"
title = "Charlas"
+++
<style>
  html,body {
    height: 100%;
    margin: 0 !important;
    padding: 0 !important;
  }
  .overflow-container {
    min-height: 100%;
    height: 100%;
    display: block;
    position: relative;
  }
  article,.entry-content,.loop-container {
    height: 100%;
  }
  .main {
    padding: 0;
    height: 100%;
  }
  /* .entry-content {
    margin: 0;
    padding: 0;
  } */
  .entry {
    height: 100%;
    width: 100%;
    margin: 0;
    max-width: 100%;
  }
  .entry-container {
    height: 100%;
    padding: 0 !important;
  }
  .entry-header {
    padding-top: 10px !important;
  }
  .speakers-container {
    height: 100%;
    display: block;
    position: relative;
  }
  .speakers-container iframe {
    width: 100%;
    height: 100%;
  }
</style>

<div class="speakers-container">
<iframe src="https://wiki2.hacklab.org.bo/hm2019/programa" />
</div>

<!-- < -->
