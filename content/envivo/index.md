+++
title = "Hackmeeting en Vivo"
+++




{{< rawhtml >}}

<video-js id=hm2019-envivo class="vjs-default-skin hm2019-envivo" controls>
  <source
 src="https://hls-endpoint.l10e.net/stream/hm0x7e3_master.m3u8"
 type="application/x-mpegURL">
</video-js>
<script src="//vjs.zencdn.net/7.5.5/video.min.js"></script>
<script>
var player = videojs("hm2019-envivo");
player.play();
</script>
<br />
<a href="https://youtu.be/vQuI3d55XA8">Fallback en YouTube</a>

{{< /rawhtml >}}
